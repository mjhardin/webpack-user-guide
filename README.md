# Examples Webpack User Guide #

These are code examples written after going through the user guide for webpack that can be found at https://webpack.js.org/guides/

## Commands ##

**Running a server via express.js**

    npm run server

**Running a dev server using webpack's watch**

    npm run watch

**Building production code**

    npm run build

These code examples stop after the section on "Caching". There are also two branches aside from master: `caching` and `code-splitting`. Those branches include code examples for the Caching and Code Splitting/Lazy Loading sections respectively.